from django.test import TestCase, Client
from django.urls import resolve

from .views import index
from .models import Status

class IndexTest(TestCase):
    def test_index_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_view(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_form_exists(self):
        response = Client().get('/')
        self.assertIn('</form>', response.content.decode())

    def test_lab6_post_success_and_render_the_result(self):
        response_post = Client().post('/', {'status':'WOOHOO!', 'name':'bisa'})
        self.assertEqual(response_post.status_code, 200)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn('WOOHOO!', html_response)
    
    def test_lab6_post_failed_and_render_the_result(self):
        response_post = Client().post('/', {'status':'', 'name':''})
        self.assertEqual(response_post.status_code, 200)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('WOOHOO!', html_response)


    def test_model_can_create_objects(self):
        status = Status.objects.create(name='ate', status='test')
        self.assertIsInstance(status, Status)
        self.assertTrue(status.status,'test')
        self.assertTrue(status.name,'ate')

    def test_submit_status(self):
        name = 'Hellow ate'
        status = 'hehehehe'
        response = Client().post('/', {
            'name' : name,
            'status' : status
        })
        self.assertEqual(response.status_code, 200)
        # self.assertIn(name, response.content.decode())
        # self.assertIn(message, response.content.decode())


# SELENIUM TEST #
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time

class IndexLiveTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        # self.selenium = webdriver.Chrome(executable_path='chromedriver_linux64/chromedriver')
        self.selenium = webdriver.Chrome(chrome_options =chrome_options)
        super(IndexLiveTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(IndexLiveTest, self).tearDown()

    def test_submitForm(self):
        selenium = self.selenium

        selenium.get(self.live_server_url)
        time.sleep(5)

        name = selenium.find_element_by_xpath("/html/body/section/form/div[1]/input")
        status = selenium.find_element_by_xpath("/html/body/section/form/div[2]/input")
        submit = selenium.find_element_by_xpath("/html/body/section/form/input[2]")

        name.send_keys('Tes')
        status.send_keys('Coba Coba')

        submit.send_keys(Keys.RETURN)
        time.sleep(1)

        assert 'Tes' in selenium.page_source
        assert 'Coba Coba' in selenium.page_source
