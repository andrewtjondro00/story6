from django import forms
from .models import Status

class NameForm(forms.ModelForm):
    class Meta:
        model = Status
        fields = ('name', 'status')