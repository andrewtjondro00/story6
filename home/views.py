from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import NameForm
from .models import Status

def index(request):
    if request.method == 'POST':
        # print(request.POST)
        form = NameForm(request.POST)
        if form.is_valid():
            status = Status(name=request.POST['name'],status=request.POST['status'])
            status.save()
    status = Status.objects.all()
    context = {'Status': status}
    return render(request, 'index.html', context)